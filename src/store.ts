import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { userSaga } from './general/user.saga';
import rootReducer from './index.reducer';

const sagaMiddleware = createSagaMiddleware();
const composedEnhancers = compose(
  applyMiddleware(sagaMiddleware),
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

export default createStore(rootReducer, composedEnhancers);

sagaMiddleware.run(userSaga);
