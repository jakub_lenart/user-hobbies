import { combineReducers } from 'redux';

import users from './general/user.reducer';

export default combineReducers({ users });
