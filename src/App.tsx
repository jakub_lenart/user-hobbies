import React from 'react';
import './App.scss';
import { MainHeader } from './general/components/MainHeader/MainHeader.component';
import UsersPage from './general/pages/usersPage';

function App() {
  return (
    <div className="App">
      <MainHeader />
      <div className="container">
        <UsersPage />
      </div>
    </div>
  );
}

export default App;
