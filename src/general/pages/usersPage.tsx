import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';

import { User } from '../model';
import { UsersInfo } from '../components/UsersInfo/UsersInfo.component';
import UserActions from '../user.action';

interface State {
  users: User[];
}

function mapStateToProps(state: State) {
  return {
    users: state.users,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return bindActionCreators(UserActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersInfo);
