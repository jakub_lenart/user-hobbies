export type User = {
  id: string;
  name: string;
  hobbies: Hobby[];
};

export type Hobby = {
  id: string;
  name: string;
  passion: Passion;
  since: number;
};

export enum Passion {
  LOW = 'LOW',
  MEDIUM = 'MEDIUM',
  HIGH = 'HIGH',
  VERY_HIGH = 'VERY HIGH',
}
