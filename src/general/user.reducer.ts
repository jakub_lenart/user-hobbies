import * as R from 'ramda';

import { User } from './model';
import { UserTypeKeys, UserTypes } from './user.action';

const initialState: User[] = [];

export default function usersReducer(state = initialState, action: UserTypes) {
  switch (action.type) {
    case UserTypeKeys.GET_USERS_SUCCESS:
      return action.payload.users;
    case UserTypeKeys.ADD_USER_SUCCESS:
      return R.prepend(action.payload.user, state);
    case UserTypeKeys.UPDATE_HOBBIES_SUCCESS:
      const userIndex = R.findIndex(R.propEq('id', action.payload.userId), state);
      const hobbiesLens = R.lensPath([userIndex, 'hobbies']);

      return R.set(hobbiesLens, action.payload.hobbies, state);
    default:
      return state;
  }
}
