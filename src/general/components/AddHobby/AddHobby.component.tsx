import React, { useState } from 'react';
import { keys, times } from 'ramda';
import * as uuid from 'uuid';

import { Hobby, Passion } from '../../model';
import styles from './AddHobby.module.scss';

type Props = {
  onAddHobby: (hobby: Hobby) => void;
};

const YEARS_TO_CHOOSE = 20;

type FormData = {
  passion: Passion | null;
  name: string | null;
  since: number | null;
};

export function AddHobby(props: Props) {
  const { onAddHobby } = props;
  const currentYear = new Date().getFullYear();

  const [hobby, setHobby] = useState<FormData>({
    passion: null,
    name: null,
    since: null,
  });

  const handleAddHobby = (e: React.FormEvent<HTMLElement>) => {
    e.preventDefault();

    const { passion, name, since } = hobby;

    if (passion && name && since) {
      const hobbyToAdd = {
        id: uuid.v4(),
        passion,
        name,
        since,
      };

      // TODO: form validation
      onAddHobby(hobbyToAdd);
    }
  };

  const handleChange = (e: React.FormEvent<HTMLElement>) => {
    e.preventDefault();

    const target = e.target as HTMLTextAreaElement;
    const { name } = target;

    const value = name === 'since' ? parseInt(target.value) : target.value;

    setHobby((prevState) => ({ ...prevState, [name]: value }));
  };

  return (
    <form className={styles.Form} onSubmit={handleAddHobby} onChange={handleChange}>
      <select name="passion" id="passion-level">
        <option disabled selected>
          Select passion level
        </option>
        {keys(Passion).map((it) => (
          <option key={it}>{it}</option>
        ))}
      </select>
      <input type="text" onChange={handleChange} name="name" id="hobbyName" placeholder="Enter user hobby" />
      <select name="since" id="hobby-since" placeholder="Enter year">
        <option disabled selected>
          Select year
        </option>
        {times(
          (i) => (
            <option key={i}>{currentYear - i}</option>
          ),
          YEARS_TO_CHOOSE
        )}
      </select>
      <button type="submit">Add</button>
    </form>
  );
}
