import React, { useState } from 'react';
import * as uuid from 'uuid';

import { User } from '../../model';
import styles from './AddUser.module.scss';

type Props = {
  onAddUser: (user: User) => void;
};

export function AddUser(props: Props) {
  const { onAddUser } = props;

  const [userName, setUserName] = useState<string>('');

  const handleAddUser = (e: React.FormEvent<HTMLElement>) => {
    e.preventDefault();

    // TODO: form validation

    userName.length > 0 && onAddUser({ id: uuid.v4(), name: userName, hobbies: [] });
  };

  const handleChange = (e: React.FormEvent<HTMLElement>) => {
    e.preventDefault();

    const target = e.target as HTMLTextAreaElement;
    const { value } = target;

    setUserName(value);
  };

  return (
    <form className={styles.Form} onSubmit={handleAddUser} onChange={handleChange}>
      <input
        type="text"
        onChange={handleChange}
        name="name"
        id="userName"
        value={userName}
        placeholder="Enter user name"
      />
      <button type="submit">Add</button>
    </form>
  );
}
