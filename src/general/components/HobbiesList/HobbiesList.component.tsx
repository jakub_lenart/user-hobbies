import React from 'react';

import { Hobby } from '../../model';
import styles from './HobbiesList.module.scss';
import { NotFound } from '../NotFound/NotFound.component';

type Props = {
  hobbies: Hobby[];
  onRemoveHobby: (hobbyId: string) => void;
};

export function HobbiesList(props: Props) {
  const { hobbies, onRemoveHobby } = props;

  return hobbies.length ? (
    <ul className={styles.List}>
      {hobbies.map((hobby) => (
        <li key={hobby.id} className={styles.ListItem}>
          <p>Passion: {hobby.passion}</p>
          <p>{hobby.name}</p>
          <p>Since {hobby.since}</p>
          <button className={styles.RemoveBtn} onClick={() => onRemoveHobby(hobby.id)}>
            remove
          </button>
        </li>
      ))}
    </ul>
  ) : (
    <NotFound text="Add hobby" />
  );
}
