import React from 'react';

import styles from './NotFound.module.scss';

type Props = {
  text?: string;
};

export function NotFound(props: Props) {
  const { text } = props;

  return (
    <div className={styles.NotFound}>
      <p>{text ? text : 'Not found'}</p>
    </div>
  );
}
