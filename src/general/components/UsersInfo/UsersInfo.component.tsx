import React, { useEffect, useState } from 'react';
import { head, prepend, propEq, reject } from 'ramda';

import { Hobby, User } from '../../model';
import { UsersList } from '../UsersList/UsersList.component';
import { HobbiesList } from '../HobbiesList/HobbiesList.component';
import { NotFound } from '../NotFound/NotFound.component';
import { AddUser } from '../AddUser/AddUser.component';
import { AddHobby } from '../AddHobby/AddHobby.component';
import styles from './UsersInfo.module.scss';

type Props = {
  users: User[];
  addUserRequest: (user: User) => void;
  updateHobbiesRequest: (userId: string, hobbies: Hobby[]) => void;
  getUsersRequest: () => void;
};

export function UsersInfo(props: Props) {
  const { users, addUserRequest, updateHobbiesRequest, getUsersRequest } = props;
  const [activeUser, setActiveUser] = useState<User | undefined>(undefined);

  const getUserById = (id: string) => head(users.filter((it) => it.id === id));

  useEffect(() => {
    getUsersRequest();
  }, [getUsersRequest]);

  useEffect(() => {
    activeUser && setActiveUser(getUserById(activeUser.id));
  }, [users]);

  const handleUserChange = (userId: string) => {
    setActiveUser(getUserById(userId));
  };

  const handleAddHobby = (hobby: Hobby) => {
    activeUser && updateHobbiesRequest(activeUser?.id, prepend(hobby, activeUser.hobbies));
  };

  const handleRemoveHobby = (hobbyId: string) => {
    activeUser && updateHobbiesRequest(activeUser?.id, reject(propEq('id', hobbyId), activeUser.hobbies));
  };

  return (
    <section className={styles.InfoWrapper}>
      <header className={styles.Header}>
        <h3>User Hobbies</h3>
      </header>
      <div className={styles.Body}>
        <div className={styles.InfoSection}>
          <AddUser onAddUser={addUserRequest} />
          <UsersList users={users} onUserSelect={handleUserChange} />
        </div>
        <div className={styles.InfoSection}>
          {activeUser ? (
            <>
              <AddHobby onAddHobby={handleAddHobby} />
              <HobbiesList hobbies={activeUser?.hobbies || []} onRemoveHobby={handleRemoveHobby} />
            </>
          ) : (
            <NotFound text="Select user" />
          )}
        </div>
      </div>
    </section>
  );
}
