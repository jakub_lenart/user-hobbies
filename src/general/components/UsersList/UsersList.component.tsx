import React from 'react';

import { User } from '../../model';
import styles from './UsersList.module.scss';
import { NotFound } from '../NotFound/NotFound.component';

type Props = {
  users: User[];
  onUserSelect: (userId: string) => void;
};

export function UsersList(props: Props) {
  const { users, onUserSelect } = props;

  return (
    <div className={styles.ListWrapper}>
      {users.length ? (
        <ul>
          {users.map((user) => (
            // TODO: highlight active user
            <li key={user.id} className={styles.ListItem}>
              <button onClick={() => onUserSelect(user.id)}>{user.name}</button>
            </li>
          ))}
        </ul>
      ) : (
        <NotFound text="Add user" />
      )}
    </div>
  );
}
