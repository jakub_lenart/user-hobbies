import React from 'react';
import styles from './MainHeader.module.scss';

export function MainHeader() {
  return (
    <header className={styles.MainHeader}>
      <div className="container">
        <h1>User Hobbies</h1>
      </div>
    </header>
  );
}
