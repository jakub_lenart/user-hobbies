import { call, put, takeEvery } from 'redux-saga/effects';

import { AddUserRequestAction, UpdateHobbiesRequestAction, UserTypeKeys } from './user.action';
import { getAllUsers, saveUser, updateHobbyList } from './user.service';

function* getUsers() {
  try {
    const users = yield call(getAllUsers);
    yield put({ type: UserTypeKeys.GET_USERS_SUCCESS, payload: { users } });
  } catch (error) {
    yield put({ type: UserTypeKeys.GET_USERS_FAILED, payload: { error } });
  }
}

function* addUser(action: AddUserRequestAction) {
  const { user } = action.payload;

  try {
    const userInfo = yield call(saveUser, user);
    yield put({ type: UserTypeKeys.ADD_USER_SUCCESS, payload: { user: userInfo } });
  } catch (error) {
    yield put({ type: UserTypeKeys.ADD_USER_FAILED, payload: { error } });
  }
}

function* updateHobbies(action: UpdateHobbiesRequestAction) {
  const { userId, hobbies } = action.payload;

  try {
    const { hobbies: updatedHobbies } = yield call(updateHobbyList, userId, hobbies);
    yield put({ type: UserTypeKeys.UPDATE_HOBBIES_SUCCESS, payload: { userId, hobbies: updatedHobbies } });
  } catch (error) {
    yield put({ type: UserTypeKeys.UPDATE_HOBBIES_FAILED, payload: { error } });
  }
}

export function* userSaga() {
  yield takeEvery(UserTypeKeys.GET_USERS_REQUEST, getUsers);
  yield takeEvery(UserTypeKeys.ADD_USER_REQUEST, addUser);
  yield takeEvery(UserTypeKeys.UPDATE_HOBBIES_REQUEST, updateHobbies);
}
