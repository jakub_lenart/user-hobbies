import axios from 'axios';

import { Hobby, User } from './model';

const { REACT_APP_JSON_SERVER } = process.env;

export function getAllUsers(): Promise<User[]> {
  return axios.get(`${REACT_APP_JSON_SERVER}/users`).then((res) => res.data);
}

export function saveUser(user: User): Promise<User> {
  return axios.post(`${REACT_APP_JSON_SERVER}/users`, user).then((res) => res.data);
}

export function updateHobbyList(userId: string, hobbies: Hobby[]): Promise<User> {
  return axios.patch(`${REACT_APP_JSON_SERVER}/users/${userId}`, { hobbies }).then((res) => res.data);
}
