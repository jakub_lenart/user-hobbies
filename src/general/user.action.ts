import { Hobby, User } from './model';

export enum UserTypeKeys {
  GET_USERS_REQUEST = 'GET_USERS_REQUEST',
  GET_USERS_SUCCESS = 'GET_USERS_SUCCESS',
  GET_USERS_FAILED = 'GET_USERS_FAILED',
  ADD_USER_REQUEST = 'ADD_USER_REQUEST',
  ADD_USER_SUCCESS = 'ADD_USER_SUCCESS',
  ADD_USER_FAILED = 'ADD_USER_FAILED',
  UPDATE_HOBBIES_REQUEST = 'UPDATE_HOBBIES_REQUEST',
  UPDATE_HOBBIES_SUCCESS = 'UPDATE_HOBBIES_SUCCESS',
  UPDATE_HOBBIES_FAILED = 'UPDATE_HOBBIES_FAILED',
}

export interface GetUsersRequestAction {
  type: UserTypeKeys.GET_USERS_REQUEST;
}

export interface GetUsersRequestSuccess {
  type: UserTypeKeys.GET_USERS_SUCCESS;
  payload: {
    users: User[];
  };
}

export interface GetUsersRequestFailed {
  type: UserTypeKeys.GET_USERS_FAILED;
  payload: {
    error: string;
  };
}

export interface AddUserRequestAction {
  type: UserTypeKeys.ADD_USER_REQUEST;
  payload: {
    user: User;
  };
}

export interface AddUserRequestSuccess {
  type: UserTypeKeys.ADD_USER_SUCCESS;
  payload: {
    user: User;
  };
}

export interface AddUserRequestFailed {
  type: UserTypeKeys.ADD_USER_FAILED;
  payload: {
    error: string;
  };
}

export interface UpdateHobbiesRequestAction {
  type: UserTypeKeys.UPDATE_HOBBIES_REQUEST;
  payload: {
    userId: string;
    hobbies: Hobby[];
  };
}

export interface UpdateHobbiesRequestSuccess {
  type: UserTypeKeys.UPDATE_HOBBIES_SUCCESS;
  payload: {
    userId: string;
    hobbies: Hobby[];
  };
}

export interface UpdateHobbiesRequestFailed {
  type: UserTypeKeys.UPDATE_HOBBIES_FAILED;
  payload: {
    error: string;
  };
}

export type UserTypes =
  | GetUsersRequestAction
  | GetUsersRequestSuccess
  | GetUsersRequestFailed
  | AddUserRequestAction
  | AddUserRequestSuccess
  | AddUserRequestFailed
  | UpdateHobbiesRequestAction
  | UpdateHobbiesRequestSuccess
  | UpdateHobbiesRequestFailed;

export function getUsersRequest() {
  return { type: UserTypeKeys.GET_USERS_REQUEST };
}

export function getUsersSuccess(users: User[]) {
  return { type: UserTypeKeys.GET_USERS_REQUEST, payload: { users } };
}

export function getUsersFailed(error: string) {
  return { type: UserTypeKeys.GET_USERS_REQUEST, payload: { error } };
}

export function addUserRequest(user: User) {
  return { type: UserTypeKeys.ADD_USER_REQUEST, payload: { user } };
}

export function addUserSuccess(user: User) {
  return { type: UserTypeKeys.ADD_USER_SUCCESS, payload: { user } };
}

export function addUserFailed(error: string) {
  return { type: UserTypeKeys.ADD_USER_FAILED, payload: { error } };
}

export function updateHobbiesRequest(userId: string, hobbies: Hobby[]) {
  return { type: UserTypeKeys.UPDATE_HOBBIES_REQUEST, payload: { userId, hobbies } };
}

export function updateHobbiesSuccess(userId: string, hobbies: Hobby[]) {
  return { type: UserTypeKeys.UPDATE_HOBBIES_SUCCESS, payload: { userId, hobbies } };
}

export function updateHobbiesFailed(error: string) {
  return { type: UserTypeKeys.UPDATE_HOBBIES_FAILED, payload: { error } };
}

export default {
  getUsersRequest,
  getUsersSuccess,
  getUsersFailed,
  addUserRequest,
  addUserSuccess,
  addUserFailed,
  updateHobbiesRequest,
  updateHobbiesSuccess,
  updateHobbiesFailed,
};
