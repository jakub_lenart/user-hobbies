### To start:
- create `db.json` file with empty users list:
`{
  "users": []
}`
- install [json-server](https://github.com/typicode/json-server) and run `json-server --watch db.json --port 3001` (or `npx json-server --watch db.json`)
- create `.env` and add `REACT_APP_JSON_SERVER=http://localhost:3001` variable
- run `yarn install`
- run `yarn start`
